# -*- coding: utf-8 -*-

# Using Splash as a javascript renderer and as a proxy server
# 把您的splash实例ip以及端口(默认8050)赋给这玩意
SPLASH_URL = 'http://127.0.0.1:8050'

# Other configurations
DUPEFILTER_CLASS = 'scrapy_splash.SplashAwareDupeFilter'
HTTPCACHE_STORAGE = 'scrapy_splash.SplashAwareFSCacheStorage'
SPIDER_MIDDLEWARES = {
	'scrapy_splash.SplashDeduplicateArgsMiddleware':100,
}
DOWNLOADER_MIDDLEWARES = {
    'scrapy_splash.SplashCookiesMiddleware':723,
    'scrapy_splash.SplashMiddleware': 725,
    'scrapy.downloadermiddlewares.httpcompression.HttpCompressionMiddleware': 810,
}
USER_AGENT = 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_12_5) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
ROBOTSTXT_OBEY = False
CONCURRENT_REQUESTS = 50
TELNETCONSOLE_ENABLED = False
BOT_NAME = 'bilibiliuser'
SPIDER_MODULES = ['bilibiliuser.spiders']
NEWSPIDER_MODULE = 'bilibiliuser.spiders'
