import scrapy
from scrapy_splash import SplashRequest
import json
import copy
import datetime
import time
import re
from bs4 import BeautifulSoup
import threading
import jellyfish
import numpy as np

from ..YDHP import YDHP_SiteInfo, YDHP_ScrapyRecorder, YDHP_ScrapySystem, YDHP_SplashRequester, YDHP_NextPages
# from .. import UnitTests, unit_test_enabled

from .. import bilibili_example_query

# 估计一下b站的用户量, 嗯大概这个数吧
estimated_users = 200000000

bilibili_users_amount = None


class BilibiliSpider0(scrapy.Spider):
    global estimated_users
    name = "bilibili_users_0"

    # 这些变量用于搜索总用户量
    next_user_id = estimated_users

    min_ghost_user_id = None
    max_real_user_id = None

    def start_requests(self):
        # 先下载一个id不存在的页面，让爬虫知道酱的页面代表着一个不存在的用户
        self.ghost_home_page = None

        # 这个用户id一定是是不存在的，不然b站就炸了
        ghost_user_id = 200000000000000000000000000
        self.m_current_user_id = ghost_user_id

        # 把这个不存在的页面爬到内存里面去
        self.splash_requester = YDHP_SplashRequester.SplashRequester()
        yield self.splash_requester.splash_requests(self.user_home_page_url(),
                                                    self.callback_download_ghost_user_home_page)

        # 然后我们就来到了response_download_ghost_user_home_page

    def __init__(self):
        super(BilibiliSpider0, self).__init__()
        self.example_query = bilibili_example_query

    def user_home_page_url(self):
        return self.example_query['example_uri'].replace(self.example_query['query_string'], str(self.m_current_user_id))

    def callback_download_ghost_user_home_page(self, response):
        self.ghost_home_page = response.text

        # 我们来看看你估计的这个用户id存在不存在
        self.m_current_user_id = estimated_users
        yield self.splash_requester.splash_requests(self.user_home_page_url(), self.callback_is_this_a_real_user_home_page)

        # 然后请看callback_is_this_a_real_user_home_page

    # 这次请求的用户id存在吗？！我们来分析一下爬来的页面与不存在的页面的相似度
    def callback_is_this_a_real_user_home_page(self, response):
        # 判断逻辑：如果这个用户存在那么这两个页面的相似度必须小于0.9
        score = jellyfish.jaro_distance(response.text, self.ghost_home_page)

        if score < 0.9:
            """
            这个用户是存在的
            """
            self.min_ghost_user_id = int(self.next_user_id + np.power(10, self.how_many_zeros(self.next_user_id)))
            self.max_real_user_id = int(self.next_user_id + np.power(10, self.how_many_zeros(self.next_user_id)))

            self.next_user_id = self.min_ghost_user_id
            self.m_current_user_id = self.next_user_id

            yield self.splash_requester.splash_requests(self.user_home_page_url(), self.callback_is_this_a_real_user_home_page)

        else:
            """
            这个用户他不存在
            """
            self.min_ghost_user_id = self.next_user_id
            self.max_real_user_id = int(self.next_user_id - \
                                    np.power(10, self.how_many_zeros(self.next_user_id)) + \
                                    np.power(10, self.how_many_zeros(self.next_user_id)-1))

            if not self.is_this_the_last_user():
                self.next_user_id = self.max_real_user_id
                self.m_current_user_id = self.next_user_id

                yield self.splash_requester.splash_requests(self.user_home_page_url(), self.callback_is_this_a_real_user_home_page)

    def how_many_zeros(self, number):
        number_of_digits = 0

        number_wrapper = [number]
        while self.divide_by_ten(number_wrapper):
            number_of_digits += 1

        return number_of_digits

    def divide_by_ten(self, number):
        if number[0] % 10 == 0:
            number[0] /= 10
            return True
        else:
            return False

    def is_this_the_last_user(self):
        global bilibili_users_amount
        if not self.min_ghost_user_id == self.max_real_user_id:
            if self.min_ghost_user_id - self.max_real_user_id == 1:
                bilibili_users_amount = self.max_real_user_id
                return True
        else:
            return False

    def __del__(self):
        with open(file='temp/bilibili_users_0.json', encoding='utf-8', mode='w') as f:
            f.write(json.dumps({"bilibili_total_users": bilibili_users_amount}))

        print(self.name + "_Finished")
        print(bilibili_users_amount)
        print("Fetched total users number above")